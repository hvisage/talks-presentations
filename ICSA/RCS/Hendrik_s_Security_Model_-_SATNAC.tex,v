head	1.10;
access;
symbols;
locks; strict;
comment	@% @;


1.10
date	2010.05.11.17.31.42;	author hendrivi;	state Exp;
branches;
next	1.9;

1.9
date	2010.05.11.12.58.54;	author hendrivi;	state Exp;
branches;
next	1.8;

1.8
date	2010.05.11.12.57.30;	author hendrivi;	state Exp;
branches;
next	1.7;

1.7
date	2010.05.09.21.22.34;	author hvisage;	state Exp;
branches;
next	1.6;

1.6
date	2010.05.09.20.58.56;	author hvisage;	state Exp;
branches;
next	1.5;

1.5
date	2010.05.09.20.41.12;	author hvisage;	state Exp;
branches;
next	1.4;

1.4
date	2010.05.09.20.34.18;	author hvisage;	state Exp;
branches;
next	1.3;

1.3
date	2010.05.09.20.19.11;	author hvisage;	state Exp;
branches;
next	1.2;

1.2
date	2010.05.09.12.47.56;	author hendrivi;	state Exp;
branches;
next	1.1;

1.1
date	2010.05.09.12.37.00;	author hendrivi;	state Exp;
branches;
next	;


desc
@@


1.10
log
@.
@
text
@\documentclass[a4paper,10pt,twocolumn]{article}

%\usepackage{pstricks}
\usepackage{graphicx}

\begin{titlepage}
  \author{Hendrik Visag\`e \\\texttt{hvisage@@gmail.com}\\Computer
    Science Department\\University of
  Pretoria}
%\\ Martin S. Olivier}
  \title{Application of the Information and Security model on
    Networked services}
\title{Network Security Dependencies - Application of an Information
  and Computer Security Model}
\end{titlepage}

\begin{document}
\maketitle
\bibliographystyle{alpha}
\begin{abstract}
  With our world becoming more and more connected, we have to face the
  Information and Computer Security(ICS) issues. Network security as a
  part of ICS, are being researched but still we are experiencing
  problems even while being considered secured at the network layer.
  We have proposed a ICS model that requires the human, environmental
  and system aspects to be sufficiently addressed before the network
  could be considered secured. We show that literature is supporting
  our model and give a brief real world example that show how the
  model should be applied to provide secured networks.
\end{abstract}

\section{Introduction}

Network security have seen the growth of different products and
services to attempt to secure the data communications and computers
systems of entities\footnote{We will use the term entity, as it is
  more than just commercial enterprises, but also government,
  parastatal and military not to mention households that is using
  networked computers to function}. But still the attacks on computer
infrastructure continues and the responses are mostly retroactive,
than proactive. This is evident in the retroactive nature of virus and
attack signatures.

The problem is that most people do not understand  Information and
Computer Security (ICS) related issues and using products alone is not
a solution, other than to create a false sense
of security. Outsourcing do not always solve the problem 
as it easily degrade into a blame shift. Research in our opinion
are mostly just addressing the symptoms and not the root causes.
It have been stated before that we can't be 100\% secure, but using
some form of risk management and security model, the security risks
could be understand and contained to desirable levels.

What we have also noticed is that the network security products make
certain assumptions that security personnel implicitly assumes, but is
not properly stated. The model discussed in
\ref{sec:liter-appl-model}, that analyzes a site's security, it
have shown that these assumptions are important and needs to be
addressed before a network could be considered secured.

In this paper, we will show the application and validation of a
proposed ICS model in relation to network security and
show that there are certain prerequisites to
be addressed and considered before a network could be considered as
secured. This is contrary to the usual security assessment of each
aspect in isolation where we could (based on properly configured
network security products) have claimed a secured network.

In the rest of this paper we a brief literature study on recent
research in ICS, followed by the proposed model. The we discuss in a bit more
detail a report and a research project that highlighted the basis of
our model's claims and how the research project actually fits into our
model. Before concluding a very brief discussion of a real world
network that was seen as secured, but according to our model were not
properly secured.


\section{Background}
\label{sec:background}

When looking at ICS conference topics and papers, the authors found
that most of the paper topics are focusing on or related to the
technical aspects of security. The human and personnel aspects are
largely neglected. In \cite{homer2009sat} is a paper that focused on a
security risk management modeller, this is the only paper the authors
have found which highlighted these aspects in research.

%A presentation by \cite{GIPI-pres} have about two slides addressing
%the human aspects, even thought they pointed out that virus attacks
%and internal hackers are the greatest threats to address.

Eloff en Eloff in \cite{eloff2003information} and
\cite{eloff2005information}, are asking for new paradigms in security
management while also comparing the different frameworks already
available. But in our opinion are still neglecting the human and
environmental aspects by not giving it the emphasis needed. Even Von
Solms mentions it so briefly in \cite{von2000information}, though
alludes to its importance for a proper ICS solution.

In \cite{o2004human}, O'Brien came to the conclusion that the United
States of America's Department of Defence's (DOD)
NIPRNet\footnote{DOD's unclassified network} is very vulnerable and
that ultimately the SIPRNet\footnote{DOD's classified network} too is
vulnerable. He based this on the DOD's CERT incidents. The paper
concludes with these statements [emphasis added]:
\begin{quote}
  Perhaps we should reconsider transformation initiatives relating to
  force structure in light of a more systematic analysis of all the
  threats to our information systems, \emph{to include the human
    threat}. There are only so many approaches to network
  security.\emph{ Human vulnerabilities underlie them all.}
\end{quote}

It is our opinion that the human aspects deserves much more emphasis
than it currently is given in security research, as if it's a known
problem, but nobody appears to be wanting to address or openly state
it.

\section{Model used}
\label{sec:model-used}

Figure \ref{fig:model} shows the basics of our model, and though it
does not include all the detail that ICS researchers would want, it is
kept to the basics to make it easy to convey the message over to
others that is not yet security conscious.
\begin{figure}[htbp] %  figure placement: here, top, bottom, or page
   \centering
   \includegraphics[scale=0.35]{Model.pdf} 
   \caption{Model for ICS}
   \label{fig:model}
\end{figure}

This model used, requires a certain threshold of security compliance
on the lower layers before the layer(s) (in this paper the network)
could be considered secured. This threshold idea (borrowed from
Maslow's hierarchy of human needs and desires) is different from
security-in-depth that assess and reports separately on each aspect.

The other aspect of this model, is that if an upper layer failed to
prevent an incident, the layers below it could and should be utilized
to detect, prevent or react to the incident. for example the host
operating system and applications should not rely on the security of
the network, but should rather handle their own security sufficiently
and be able to handle failures from the network security devices. The
network security in this regard should augment, not be the primary
security.

We will briefly explain the model's different layers from the bottom
up.

\subsection{Human aspects}
\label{sec:human-aspects}

This will be the full scope including education and training of
personnel, trust aspects of personnel, policies, procedures. Top level
management must have and active involvement in ICS at this layer. All
personnel should be security conscious as they are the first instance
in proactive and the last resort in reactive defense.

\subsection{Environmental aspects}
\label{sec:environment}

This is easily overlooked in security, especially when disaster
recovery does not include the security department. The physical
security of the network cables, computer terminals and backup devices
and tape storage, should all be part of the ICS framework and
policies.  This is all but an exhaustive list of things to consider at
this layer as smart phones and other portable devices are entering this
arena rapidly and research on coping with that influx have not yet
been noticed by the authors.

\subsection{Computing platforms}
\label{sec:systems}

At this layer we group the host operating systems, applications and
other devices providing data or services for an entity to function. It
is perhaps too wide a definition, but for simplicity in applying this
model we will abstain from adding too much detail. Also it will be
stated that a host and the applications running on it, are for most
users a single service that is not perceived as separate entities, as
the security and availability of the one is dependent on the security
and availability of the other.
  
\subsection{Communications}
\label{sec:communications}

This is the network and other means of data exchange between
systems. These systems could be sharing the same host, separated in a
virtualized environment on different virtual hosts but same physical
host, the same data centre, or on the other side of the world (if not
in outer space).\footnote{A case could be made to split this layer
  between inter and intra entity communications}

\subsection{Mathematical aspects}
\label{sec:maths}

Cryptography and models are grouped into this layer. Although some
authors view cryptography as the basis for secure networks, we contend
that the appropriate use of cryptography will only have the desired
effect, once the layers below it have been secured. The same would
hold of any other ICS model applied to a computing system. 


\section{Human-Centric Network security}
\label{sec:human-centr-netw}

Most\'efaoui and Br\'ezillon describes a semantics based access
control system in \cite{mostefaoui2006human}, where security contexts
is the basis for the decisions made on user access to resources. They
show a method to model the access to patient information based on who,
from where as well as the time of day the request is made. They
specifically state that this system is not a replacement for firewalls
or IDS, but a system to augment the security of the applications.

The basic idea behind this model, is to understand the semantics of
users that need access to resources and when they are allowed such
access. These semantics is then modeled in relation to the user and
from where they access the data. This already highlights for us the
need to have the human and environmental aspects secured before such a
system or model would be effective in its application.


The loophole in the example given in \cite{mostefaoui2006human} is
that a normal nurse could access to the patient records without
notifications, just by being accessing from the emergency room
terminal. Contrast this to the access by the same nurse from inside
the hospital that would have sent a notification to the treating
physician. The assumption (though not stated) is that the emergency
room terminal would be secured (environmental factor) and that the
nurse (human factor) have not slipped her access password.

As should be obvious to the reader is that the basis of the triangle
from figure \ref{fig:model} have to be satisfied sufficiently before
this model would be sensible in it's application to secure the
networked resources as desired.


\section{DOD investigation}
\label{sec:dod-investigation}

O'Brien states in \cite{o2004human} that cryptography is the strongest
tool for controlling most security threats, but then continues to show
how the unbreakable Enigma cipher machine's codes were broken because
\begin{quote}
  \ldots sloppy user behavior gave British code breakers critical
  help. It was the people, not the technology, that undermined
  Enigma.\emph{ Could the same thing happen today?}[emphasis
  added]\end{quote}

O'Brien continues to quote statistics to show the trusted insider
(employees or other insiders with access to computers) to be the
biggest security threat to entities. Though specifically DOD focused,
O'Brien gave statistics about individuals caught for espionage as late
as 1999. If this is still happening after the Cold-war is over, there
would (by human greed) definitely also be several cases involving
industrial espionage. And it does not matter if the network is secured,
the human can still print out the data, and instead of shredding, post
it to the adversary.

O'Brien continues and refers to Smith \cite{smith1998electronic} and
contends that even with break-ins from outside attackers, the need for
inside information is still there to be able to get to the databases
and other devices. O'Brien's recommendations is that \emph{personnel
  be sufficiently trained and that annual refresher training be
  implemented}, before users are given access by managers and leaders
to computer infrastructures.
\begin{quote}
  Although most people in the military, the government, and corporate
  America work with information systems, computer security is still
  practiced half-heartedly. [\cite{smith1998electronic} as cited by
  \cite{o2004human}]
\end{quote}

O'Brien continued to point out other factors that leaders and managers
should consider, like:
\begin{itemize}
\item aggressive physical security
\item internal compartmentalization
\item screen users with access to critical networks
\item supervisors given the rights to punish transgressors
\item revoking privileges prior to lay-off announcements
\end{itemize}

O'Brien points out that ``\emph{Human vulnerabilities cannot be solved
  with technological solutions alone}'' and that the insider problem
should be addressed with human solutions. This fits perfectly in with
our proposed model that requires the human layer to address failures
from the upper technology layers. O'Brien's further states that the
insider factor is largely mitigated ``when there is intervention by
supervisors, co-workers and friends and family''.

O'Brien starts his closing by stating that the nature of the DOD and
military, the changes needed to educate the personnel in proper
security, often takes a back seat to operational needs. We would make
the claim that it is a general corporate trend too and part of the
reason that networks have security issues, as the general personnel
are lacking the knowledge and forethought they need.

As O'Brien states that there will always be problems and you can not
make a network 100\% secure, it will be making a difference as the
humans are continually made aware of their roles through refresher
courses.

\subsection{Validation of Model}
\label{sec:appl-report-model}

O'Brien's \cite{o2004human}, is the report that validates the bottom
two layers of our model especially in the network security arena.

Referring again to figure \ref{fig:model}, the human aspects at the
bottom will degrade the security of the network and systems if not
sufficiently addressed. Coupled with physical security controls
(environmental aspects) you can then set a basis for building secure
systems on top of them.

Human aspects is the foundation of any secure system.


\section{SAT solving experiments}
\label{sec:liter-appl-model}

Homer and Ou explain in \cite{homer2009sat}, the use of Boolean
Satisfiability Solving (SAT Solving) technique to model and reason
about the security of an entity's network. They used three solving
techniques called MulVAL, UnVAL and MinCostSAT in their experiments
and model. 

First they used MulVAL to generate a proof graph of the attack paths
an attacker may use to gain access to critical resources. Their system
then reasons about these using MinCostSAT to minimize the security
threats while maximizing the usability. Finally they examined the
UnSAT core to reduce the options to straightforward choices in
achieving the desired security and usability levels.

Their experiment was based on a real life utility provider's network
with the usual firewalls, VPN and centralized Citrix based
servers to get access to certain devices in the control network from
inside the entity and from the outside through the Internet.

The most interesting point to note of their experiment (from a network
security perspective) were not the firewalls that got flagged as
problematic by their solvers, but the systems and users. In essence
the first iteration wanted to shut down most of the hosts. Once they
tuned the values for usability of the systems, the MinCostSAT still
advised them to fix the vulnerabilities of the Web server and to
address user behaviour to follow proper security procedures. The same
type of output were obtained using their UnSAT core. This again is
in line with views of Schneier \cite{schneier2000secrets}.

%\subsection{Discussion and application of model}
%\label{sec:disc-appl-model}

Though Homer and Ou's experiments in \cite{homer2009sat} could have
been geared towards the human elements to give the results
above. However, looking at the diagram presented in their paper,
nothing wrong or lacking\footnote{other than perhaps an IPS} could be
found. It is our opinion that their results are in line with our model
that requires a certain threshold of human effort and
understanding of ICS. Even the systems layers needed attention before the
network layer could be considered to be secured.

It is our opinion that Homer and Ou's analysis methods, merged with
our model as educational tool, would be a promising security analysis
and management platform.

\section{Real life example of application}
\label{sec:real-life-examples}

Now that we have looked at the model, some reported issues and
experiments in network security, we will briefly examine a rel life
experience the author had at a major corporate,
where the human and environmental aspects negatively impacted an
otherwise ``secure'' network.

\subsection{Corporate Example}
\label{sec:corporate-example}

At this one enterprise, we had a stringent policy of password changes,
remote access that is granted based on your cellphone SIM, and a very
stringent WiFi policy and security setup. Only some floors were
allowed unfettered access through the data centre firewalls and the
official policy was that only SSH (no telnet) were to be used for
access to the data centre machines. Microsoft Windows machines were
also forced to have updates and the corporate anti-virus installed. All
in all a good security setup.

However, the environment were not properly secured. The network points
in the boardrooms were allowed access through the firewalls (they were
on the same floors as the rest of the allowed personnel) and these
boardrooms were not behind the same stringent access controlled doors.

Even worse, the floors where the access control doors are on, were a
work area for high turn over contractors. This made physical access
control close to unenforceable, as you just knocked on the glass door,
and somebody will ``badge'' you in and out.

Another human and system related legacy, left the Unix team with a
host only authentication scheme and too many machines to manage. This
then degraded to a situation where a shared account was used for
system administration between the administration staff that had high
turn-overs themselves for various personal reasons.

These all added up to an incident that we could just state the
perpetrator originated a connection from that floor in that building.
Since DHCP logs were not available, no cameras installed to the
boardrooms and the physical access control non-existent, there were no
way we could even remotely track a possible suspect.

\subsection{Application of model }
\label{sec:appl-model-example}

In this example, we would advise the following remedies based on the
model presented:

\subsubsection{Human remedies}
\label{sec:human-remedies}


\begin{itemize}
\item Educate personnel to not allow strangers access to restricted
  areas
\item Educate administration staff on proper administration procedures
\item Management to engage with the administrators to understand their needs and concerns
\item Management to draw up proper contractor access policies and
  procedures and to enforce it
\item Contractors to be briefed and educated about policies and
  procedures before allowed access to the network 
\end{itemize}

\subsubsection{Environmental changes}
\label{sec:envir-chang}

\begin{itemize}
\item Boardrooms with no access control to be identified and network
  connections removed or properly secured through other security means
\item Contractors to be provided their own workspace away from the
  secured network.
\item Access control logs to be available when tracking incidents.
\item No laptops to be allowed on site unless properly audited and documented.
\end{itemize}

\subsubsection{System changes}
\label{sec:system-changes}

\begin{itemize}
\item Installation of a centralized account system.
\end{itemize}

\section{Future work}
\label{sec:future-work}

As the paper got reviewed, the author have been made aware of the
COBIT maturity model. More research should go into the applicability 
of the COBIT and proposed models to each other and how they could be
integrated into each other.

\section{Summary}
\label{sec:summary}

In this paper we have briefly presented a proposed ICS model
consisting of layers that need a certain threshold of compliance
before the layers above it could be considered secured. We also
showed that reports and experiments in literature validates this model
from especially a network security perspective. 

We also presented a real life experience that showed how the bottom
layers degraded an otherwise ``secured'' network, and showed that the
human and environmental aspects needs to be considered in securing
networks.


\section{Conclusion}
\label{sec:conclusion}

This paper applied a proposed model to network security and showed it
to be valid in the threshold assumptions of the lower layers (in this
case human, environmental and computing systems) in
securing the upper layers (in this case networks).

\bibliography{model}

\appendix

\section{Biography}

Hendrik Visag\`e received a BSc(Computer Science) from Univ. of Pretoria in
1994. Since then he have been employed in various roles where security
was part of his work. He was involved in major firewall
deployments and briefly worked at NedCor's Information
Security. Currently full time BSc(Hons)(Computer Science) student
at University of Pretoria   
\end{document}
@


1.9
log
@spell checking
@
text
@d270 1
a270 1
  practiced half-heartily. [\cite{smith1998electronic} as cited by
@


1.8
log
@spell checking
@
text
@d270 1
a270 1
  practiced half-heartedly. [\cite{smith1998electronic} as cited by
@


1.7
log
@Close to 2nd hand in
@
text
@d8 1
a8 1
    Science department\\University of
d13 2
a14 2
\title{Network security dependencies - Application of an Information
  and computer Security model}
d92 1
a92 1
Eloff en Elof in \cite{eloff2003information} and
d100 1
a100 1
In \cite{o2004human}, O'brien came to the conclusion that the United
d104 1
a104 1
vulnerable. He based this on the DoD's CERT incidents. The paper
d125 1
a125 1
others that is not yet security concious.
d135 2
a136 1
could be considered secured. This threshold idea is different from
d139 1
a139 1
The other aspect of this model, is that if a upper layer failed to
d157 1
a157 1
personnel should be security concious as they are the first instance
d168 1
a168 1
this layer as smartphones and other portable devices are entering this
d179 1
a179 1
stated that a host and the applications runnning on it, are for most
d348 1
a348 1
inline with views of Schneier \cite{schneier2000secrets}.
d357 1
a357 1
found. It is our opinion that their results are inline with our model
d486 1
a486 1
Hendrik Visag\`e received a BSc(ComputerScience) from Univ. of Pretoria in
d490 1
a490 1
Security. Currently full time BSc(Hons)(ComputerScience) student
@


1.6
log
@re- Added lost edits
@
text
@d408 1
a408 1
\subsection{Application of model to example}
d448 22
d474 4
a477 3
This paper applied a proposed model to network security, highlighting again
the human, environmental and systems aspects that should be sufficiently
addressed before a network could be claimed to be secured.
@


1.5
log
@some more edditing
@
text
@d256 2
a257 1
the human can still print, and instead of shredding, post it.
d259 7
a265 7
AS O'Brien continues, he refers to Smith \cite{smith1998electronic}
and contends that even with breaks from outside attackers, the need
for inside information is still there to be able to get to the
databases and other devices. O'Brien's recommendations is that
\emph{personnel be sufficiently trained and that annual refresher
  training be implemented}, before users are given access by managers
and leaders to computer infrastructures.
d286 4
a289 4
the proposed model that requires the human layer to address failures
from the upper. O'Brien's states the that the insider factor is
largely mitigated ``when there is intervention by supervisors,
co-workers and friends and family''.
d295 2
a296 2
reason that networks have security issues, as the personnel are
lacking the knowledge and forethought they need.
d303 1
a303 1
\subsection{Application of report to Model}
d306 2
a307 2
This \cite{o2004human} is the report that validates the application
and bottom two layers of our model in the network security arena.
d321 12
a332 8
\cite{homer2009sat} explained the use of a Boolean Satisfiability
Solving (SAT Solving) technique to model and reason about the security
of an entity's network. This is done by using MulVAL to generate a
proof graph of the attack paths an attacker may use to gain access to
critical resources. Their system then reasons about these using
MinCostSAT to minimize the security while maximizing the usability and
by examining the UnSAT core to reduce the options to straightforward
choices.
d335 3
a337 2
with the usual type of firewalls, VPN and centralized Citrix based
server to get access to certain devices in the control network.
d349 2
a350 2
\subsection{Discussion and application of model}
\label{sec:disc-appl-model}
d352 12
a363 11
\cite{homer2009sat} experiments could have been geared towards the human
elements to give the results above. However, looking at the diagram
presented in their paper, nothing wrong or lacking\footnote{other than
perhaps an IPS} could be found. It is our opinion that their results
are inline with our model that requires a certain threshold of human
effort and understanding. Even the systems layers needed attention
before the network layer could be shown/proved to be secured.

Looking at \cite{homer2009sat}'s analysis method, it will be our opinion
that merged with our model as educational tool, it could be the basis
for an excellent security analysis and management platform.
d365 1
a365 1
\section{Real life examples of application}
d369 2
a370 1
experiments in network security, we will give some brief examples
d380 1
a380 1
allowed unfettered access throw the data centre firewalls and the
d386 1
a386 1
However, the environment weren't properly secured. The network points
d388 2
a389 2
on the same floors as the rest of the allowed personnel), and typically
these boardrooms were not behind access control doors.
@


1.4
log
@some more editing
@
text
@a213 6
Although \cite{mostefaoui2006human} put the enforcer separate from the
resources, I would content that the better place for it would be
inside the application, however, such a system could be used very
effectively in other enforcement points in critical networks to assess
the rights and privileges of users given the security contexts.

d216 14
a229 11
access. But yet again, it highlights for us the need to have the human
and environmental aspects secured before such a system would be able
to add value. 

The loophole in the example given in \cite{mostefaoui2006human} is the
access to the patient records by a normal nurse from the emergency
room is allowed without notifications. Contrasted to the access by the
same nurse from inside the hospital that would sent a notification to
the treating physician. The assumption (though not stated) is that the
emergency room terminal would be secured (environmental factor) and
that the nurse (human factor) have not slipped her access password.
d232 3
a234 3
from figure \ref{fig:model} have to be satisfied sufficiently before this
model would be sensible in it's application to secure the network as
needed.
@


1.3
log
@some more editing
@
text
@d84 3
a86 3
largely neglected. In \cite{homer2009sat} is a paper
that focused on a security risk management modeller, this is the only paper the
authors have found which highlighted these aspects in research.
d95 4
a98 4
available. But in our opinion are still neglecting the human and environmental
aspects by not giving it the emphasis needed. Even Von Solms mentions it so briefly
in \cite{von2000information}, though alludes
to its importance for a proper ICS solution.
d110 2
a111 2
  threat}. There are only so many approaches to network security.\emph{
    Human vulnerabilities underlie them all.}
d114 4
a117 3
It is our opinion that the human aspects deserves much more emphasis than
it currently is given in security research, as if it's a known
problem, but nobody appears to be wanting to address or openly state it.
d133 8
a140 9
This model used, requires a certain threshold of security
compliance on the lower layers before the layer(s) (in this paper the
network) could be considered secured. This threshold idea is different from  
security-in-depth that assess and reports separately on each
aspect. 

The other aspect of this model, is that if a upper layer failed to prevent
an incident, the layers below it could and should be utilized to
detect, prevent or react to the incident. for example the host
d143 3
a145 2
and be able to handle failures from the network security devices. The network
security in this regard should augment, not be the primary security.
d175 7
a181 7
other devices providing data or services for an entity to
function. It is perhaps too wide a definition, but for simplicity in
applying this model we will abstain from adding too much detail. Also
it will be stated that a host and it's applications are for most users
a single service that is not perceived as separate entities, as the security and
availability of the one is dependent on the security and availability
of the other.
d193 1
a193 1
\subsection{Maths}
d196 5
a200 6
This is the layer where cryptography and other models fits in. Some
authors view cryptography as the basis for secure networks, but we
contend that cryptography only have a sensible place once the
(especially intra) entity communications network and systems have been
properly secured. The judicial use of cryptography in our view will
then give the benefits required.
d207 4
a210 4
control system in \cite{mostefaoui2006human}, where security contexts is the
basis for the decisions made on user access to resources. They show a
method to model the access to patient information based on who, from
where as well as the time of day the request is made. They
d226 5
a230 5
The loophole in the example given in \cite{mostefaoui2006human} is the access
to the patient records by a normal nurse from the emergency room is
allowed without notifications. Contrasted to the access by the same
nurse from inside the hospital that would sent a notification to the
treating physician. The assumption (though not stated) is that the
d261 7
a267 7
AS O'Brien continues, he refers to Smith \cite{smith1998electronic} and
contends that even with breaks from outside attackers, the need for
inside information is still there to be able to get to the databases
and other devices. O'Brien's recommendations is that \emph{personnel be
  sufficiently trained and that annual refresher training be
  implemented}, before users are given access by managers and leaders
to computer infrastructures.
d271 2
a272 1
  practiced half-heartedly. [\cite{smith1998electronic} as cited by \cite{o2004human}]
d286 1
a286 1
with technological solutions alone}'' and that the insider problem
d297 2
a298 2
reason that networks have security issues, as the personnel are lacking
the knowledge and forethought they need.
d308 2
a309 2
This \cite{o2004human} is the report that validates the application and
bottom two layers of our model in the network security arena.
d311 2
a312 2
Referring again to figure \ref{fig:model}, the human aspects at the bottom
will degrade the security of the network and systems if not
d323 4
a326 4
\cite{homer2009sat} explained the use of a Boolean Satisfiability Solving
(SAT Solving) technique to model and reason about the security of an
entity's network. This is done by using MulVAL to generate a proof
graph of the attack paths an attacker may use to gain access to
@


1.2
log
@some editing
@
text
@d243 5
a247 4
O'Brien states in \cite{o2004human} that cryptography is the strongest tool for controlling
most security threats, but then continues to show how the unbreakable
Enigma cipher machine's codes were broken because
\begin{quote}\ldots sloppy user behavior gave British code breakers critical
d252 8
a259 8
O'Brien continues to quote statistics to show the trusted insider (employees
or other insiders with access to computers) to be the biggest security
threat to entities. Though specially DOD focused, gave statistics
about individuals caught for espionage as late as 1999. If this is
still happening after the Cold-war is over, there would (by human greed)
definitely also be several cases involving industrial espionage. And
doesn't matter if the network is secured, the human can still print,
and instead of shredding, post it.
@


1.1
log
@Initial revision
@
text
@d153 4
a156 3
personnel, trust aspects of personnel, policies, procedures and top
level management's involvement in ICS. All personnel should be
security concious as they are the last resort in reactive defense.
d158 1
a158 1
\subsection{Environment}
d161 8
a168 7
This is easily overlooked in security, as disaster recovery is not
always seen as a security department function. The physical security
of the network cables, computer terminals and backup devices and tape
storage, should all be part of the ICS framework and policies.
This is all but an exhaustive list of things to consider at this
layer as Pads and smartphones are entering this arena rapidly and
research on coping with that influx is serious lacking.
d170 1
a170 1
\subsection{Systems}
d174 1
a174 1
other devices providing data or services for the entity to
d178 1
a178 1
a single service that can't be seen as separate, as the security and
@
