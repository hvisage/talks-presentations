
                                                                     
                                                                     
                                             
========================================================================
Review Form - Submission #443
=======================================DZE3CTX9=443=====================

Title: Investigating the Performance and Code
       Characteristics of Three Parallel Programming
       Models for C++ 

Authors: Waide Tristram and Karen Bradshaw  

-------------------------------------------------------------------------
SECONDARY REVIEWER
-------------------------------------------------------------------------

If you used a secondary reviewer to help with this review, and you wish 
to give credit to this person, enter his/her name after the colon (:) sign 
below.  If no secondary reviewer was used, leave the field blank.  

  Secondary Reviewer: Hendrik Visage

                                         =7XD3914=SecondaryRev===========


-------------------------------------------------------------------------
SCORED EVALUATION
-------------------------------------------------------------------------



     ====================================================================
     Originality of paper
     =================================================9YEDC25=S=1========

     Insert your evaluation directly following the colon (:) sign.
     Please select an INTEGER (whole-number) score from within the
     range specified.

     The form has been initialized with the minimum score as a
     default.  Replace this default with your own evaluation.

     Originality of paper (1 - 5): 3

     =7XD3914=S=1

     ====================================================================
     Technical quality of paper
     =================================================9YEDC25=S=2========

     Technical quality of paper (1 - 5): 3

     =7XD3914=S=2

     ====================================================================
     References
     =================================================9YEDC25=S=3========

     References (1 - 5): 3

     =7XD3914=S=3

     ====================================================================
     Presentation style
     =================================================9YEDC25=S=4========

     Presentation style (1 - 5): 3

     =7XD3914=S=4

     ====================================================================
     Interest to SATNAC
     =================================================9YEDC25=S=5========

     Interest to SATNAC (1 - 5): 1

     =7XD3914=S=5

     ====================================================================
     Programme topic
     =================================================9YEDC25=S=6========

     Programme topic (1 - 5): 1

     =7XD3914=S=6

     ====================================================================
     Overall Opinion
     =================================================9YEDC25=S=7========

     Overall Opinion (1 - 4): 1

     =7XD3914=S=7

-------------------------------------------------------------------------
DETAILED COMMENTS
-------------------------------------------------------------------------

Please supply detailed comments to back up your rankings. These
comments will be forwarded to the authors of the paper.

Enter your comments after the "Start" boundary line. Use as many lines
as you need, but make sure that the "End" boundary marker directly
follows the last line of your detailed comments.

=Start==============================================9YEDC25=comments=====

Okay, a factual error: The tests were done using C++ and pointing out
that the Pthread library is to be C specific (this so far is correct),
though the claims are made in the introduction to utilize this for
Asterisk (www.asterisk.org). Looking at the source on
http://svnview.digium.com/svn/asterisk/trunk/ (and having worked on
that code base 2-3years ago :( ), Asterisk PBX is a pure C and not C++
based project so these tests would not help Asterisk ;( Asterisk
already use some Pthreads as could be gleamed from:
 http://svnview.digium.com/svn/asterisk/trunk/main/threadstorage.c

Have consideration been given to:
 FastFlow: http://en.wikipedia.org/wiki/Fastflow_%28computing%29
 OpenThreads: http://openthreads.sourceforge.net/


Mandelbrot set: This set is interesting only between real: -2 and 0.5
and imaginary: -1 to 1 The "dimension" given would've been only in the
+ + quadrant, and thus would've bailed out in the first iteration of
most of the space if not scaled. I would've expected a much bigger
percentage in the g++-complex area compared to the inner loop, which
adds to my suspicion about the space examined for the Mandelbrot
calculations.

Also, were floating point or integer optimizations used for these
calculations? You did mention the SSE rewrite, but the impact of
different compiler optimizations weren't mentioned. Having done a C++
benchmark myself this semester, there are interesting aspects to the
top-end optimizations that the compilers provide, and then there are
also the Intel Optimized math libraries.

Spending the time to do some benchmarks and code profiling myself
(only having MacOSX available given the time for the review), I got
similar (though different methods for statistics gathering) on the
Mandelbrot set using the Apple GCC 4.2.1 and the code analyzer also
point out the use of SSE instructions for the inner-loop.

The "problem" with using the Mandelbrot set for this benchmark, is
that the FPU instructions in current commodity CPUs are all typically
executing in a clock cycle, not like in the XT (8088) till 80386 were
there FPU were separate and took multiple clock cycles to complete and
we had to revert to integer methods to be able to calculate these in
reasonable times (and using bailouts of typically 100 iterations, not
like my quick tests with bailouts of 1000 iterations). What I would
really like to see in these tests would be the use of Deep Zoom
(http://www.nahee.com/spanky/www/fractint/dz.html) where you will need
to work with much bigger precision more than that offered by the FPUs
of current generation commodity CPUs -- then you might get a >90% use
of the inner most calculation loop, and then the overheads of the
multithreading methods in my opinion would be better gauged.

This do brings us to an interesting phenomenon that I believed you
have hit in your static vs. dynamic scheduling: The overhead is not
that much lesser than the actual execution units. Take the case for
compiling source code using GNU make's -j option. The "rule" is (was?)
that you issue a value of (number of CPUs + 1) to be parallelized, as
you have disk reads, writes and other processor overhead, to be able
to keep all the CPUs busy at 100%.

Your tests and observations related to SSE instructions, is
distracting from the tests related to the multi threading, as you are
introducing yet another variable and thus not testing the efficiency
of the multi threading algorithms, but the optimization on the
hardware platform available.

To be honest, for a telecommunications conference, I would've rather
wanted to see an HTTP or AGI (Asterisk's CGI for call
handling/decisions) type server's performance using the different
thread models, than to compare a computational system that I would
suspect fits better with computational intelligence or rendering
systems.

That was my concerns.


The paper does a good job in examining and presenting the issues and
concerns relating to comparing certain multi threading models
available for the Unix based systems, and the results also have been
interesting to look at, especially the code changes and intrusiveness
of the different frameworks.

I would rather advise (given the Mandelbrot set used in the tests) to
present this paper to a conference related to computationally
intensive needs and applications were these results would be adding
considerable value.


Last paragraph of Conclusion should rather be moved to a “related
works” section.

Grammar/Spelling:

P2, C 3) 1st Par last line: ".. care must *be* taken.."


=End================================================7XD3914=comments=====




-------------------------------------------------------------------------
CONFIDENTIAL COMMENTS FOR COMMITTEE
-------------------------------------------------------------------------

You may wish to withhold some comments from the authors, and include
them solely for the committee's internal use.  You may enter your
confidential comments in the area below (using as many lines as you
need). If you don't have confidential comments for this submission,
leave the area blank.


=Start==========================================9YEDC25=confComments=====

The "factual error" mentioned, is in my opinion a method to convince
people to include it in Satnac, and showed a quick "insertion" and not
a thorough research into the applicability to the Asterisk system and
Satnac in general. Even if that paragraph/reference is removed and a
better option researched, I would still be questioning the
telecommunications application given the test method used and given my
experience in that field.


=End============================================7XD3914=confComments=====




========================================================================
End Review Form - Submission #443
=======================================CX39VB8D=443=====================

