\select@language {english}
\beamer@sectionintoc {1}{Introductions}{7}{0}{1}
\beamer@subsectionintoc {1}{1}{Who is this clown in front?}{8}{0}{1}
\beamer@subsectionintoc {1}{2}{.. and what does it believe?}{9}{0}{1}
\beamer@subsectionintoc {1}{3}{What emotions?}{15}{0}{1}
\beamer@sectionintoc {2}{Back to the 90s}{25}{0}{2}
\beamer@subsectionintoc {2}{1}{Coming from the 80s}{27}{0}{2}
\beamer@subsectionintoc {2}{2}{Internet !!}{29}{0}{2}
\beamer@subsectionintoc {2}{3}{NSA in the 90s}{31}{0}{2}
\beamer@subsectionintoc {2}{4}{PGP, DVD-CSS, export grade and free speech}{32}{0}{2}
\beamer@subsectionintoc {2}{5}{S.K.I.P. and IPSec's IKE start}{33}{0}{2}
\beamer@sectionintoc {3}{Security model history}{34}{0}{3}
\beamer@subsectionintoc {3}{1}{B.B.I. and corporations}{35}{0}{3}
\beamer@subsectionintoc {3}{2}{Just Slap on a Crypto VPN}{37}{0}{3}
\beamer@subsectionintoc {3}{3}{Let's try and be pragmatic first}{39}{0}{3}
\beamer@sectionintoc {4}{And today?}{40}{0}{4}
\beamer@subsectionintoc {4}{1}{9/11}{41}{0}{4}
\beamer@subsectionintoc {4}{2}{NSA - Snowden leaks}{42}{0}{4}
\beamer@subsectionintoc {4}{3}{RICA/FICA - recordings}{44}{0}{4}
\beamer@subsectionintoc {4}{4}{Privacy, What privacy?}{47}{0}{4}
\beamer@sectionintoc {5}{Scenarios}{48}{0}{5}
\beamer@subsectionintoc {5}{1}{Land of Knowalian}{49}{0}{5}
\beamer@subsectionintoc {5}{2}{File transfer(s)}{53}{0}{5}
