/*global CN, console, window, location, document, jQuery, setTimeout */
/*jslint plusplus: true */
if (typeof CN === 'undefined' || !CN) {
    throw ('CN and/or jQuery library is missing!');
}

/**
 * @class    CN ad
 * @public
 * @memberOf CN
 * @author   Ars Technica
 */
CN.ad = CN.ad || {};

/**
 * Ars Accelerator DFP plugin.
 * Extend current kw-set with kws from pulled from Accelerator
 *
 * @requires    CN.dart
 * @requires    CN
 * @requires    jQuery
 * @author      Ars Technica
 */
CN.ad.arsaccelerator = (function ($, $CNd, $D) {
    var
        ready             = false,

        protocol          = location.protocol || 'http:',

        timeout           = 800,

        apiUrl            = '//dff7tx5c2qbxc.cloudfront.net/hot/',

        /**
         * Grab the JSON from Accelerator app. If request
         * is successful, register the plugin with CN.dart.
         * @private
         */
        getData = function () {
            try {
                var parsely = $('meta[name="parsely-page"]'),
                canonical = encodeURIComponent($.parseJSON(parsely.attr('content')).link);
            }
            catch(err) { // if meta is not present or malformed, let's stop the plugin
                finished(false);
                $D.info(plugin.name + ':  plugin disabled - metadata not found.');
                return false;
            }
            $.ajax({
                url         : protocol + apiUrl + CN.dart.get('site') + '/' + canonical,
                dataType    : 'script',
                timeout     : timeout,
                error       : function (x, t) {
                    finished(false);
                    $D.info(plugin.name + ' plugin disabled', ['script ' + t, 'using site code ']);
                },
                cache       : true,
                success     : function (data) {
                    ready = true;
                    parse();
                },
                complete    : function () {
                    $D.info(plugin.name + ' Ars Accelerator call complete');
                }
            });
            return true;
        },


        init = function (obj) {
            CN.ad.arsaccelerator.kws = [];
            getData();

            // Schedule parse to run in 5 seconds as a failsafe
            setTimeout(function() {
                if (!plugin.isFinished && !ready) {
                    parse();
                }
            }, 5000);
        },

        finished = function (ret) {
            var
                i,
                len = plugin.callbacks.length;

            plugin.isFinished = true;
            $(window).trigger('CN.customEvents.dartPlugin', [plugin, ret]);

            for (i = 0; i < len; i++) {
                plugin.callbacks[i]["func"].apply((plugin.callbacks[i]["scope"] || null), (plugin.callbacks[i]["args"] || []));
            }
        },

        /**
         * Parse the Ars Accelerator JSON object, extract usable kws, and
         * return an ad object with the kws appended for CN.dart validation.
         * @private
         */
        parse = function () {
            var
                i,
                len,
                kws = CN.ad.arsaccelerator.kws,
                ret = [];

            if (!kws) {
                $D.warn("Ars Accelerator Request Timed Out", ["Setting plugin to finished to proceed to render ads"]);
                return finished(false);
            }

            len = kws.length;
            for (i = 0; i < len; i++) {
                ret.push(kws[i]);
            }

            finished ({
                ad : {kws : $CNd.get('ad').kws.concat(ret)}
            });
        },

        plugin = {
            init       : init,
            name       : 'CN Ad Ars Accelerator kws',
            modifies   : ['keywords'],
            requires   : [],
            callbacks  : [],
            isFinished : false
        },

        register = function () {
            $CNd.register(plugin);
        };


    // Register immediately.
    register();


    return {
        tags : false
    };
}(jQuery, CN.dart, CN.debug));
