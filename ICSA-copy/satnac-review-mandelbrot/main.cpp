#include <iostream>
#include <complex>
#include <saturn.h>

int main (int argc, char * const argv[]) {
    // insert code here...
	const double left=1,
			right=2,
			top=2,
			bottom=1;
	const int steps=1000;
	const double xsteps=(right-left)/steps,
	ysteps=(top-bottom)/steps;
	startSaturn();
	for (double x=left; x<right; x+=xsteps) {
		for (double y=bottom; y<top; y+=ysteps) {
			std::complex<double> z(x,y);
			int n=0;
			//std::cout<<z;
			while ((n<1000) & (abs(z)<2)) {
				z=z*z+z;
				n++;
			}
			//std::cout<<n<<" ";
		}
		//std::cout<<"x++\n";
	}
	stopSaturn();
	//std::complex<double> x(0.5,0.5);
    //std::cout << "Hello, "<<x*x*x*x<<" World!\n";
    return 0;
}
