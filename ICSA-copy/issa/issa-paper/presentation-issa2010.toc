\beamer@endinputifotherversion {3.07pt}
\select@language {english}
\beamer@sectionintoc {1}{Motivation}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Wrong security focus}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Need to explain}{10}{0}{1}
\beamer@sectionintoc {2}{Model }{13}{0}{2}
\beamer@subsectionintoc {2}{1}{Main Results}{13}{0}{2}
\beamer@subsectionintoc {2}{2}{Basic Ideas for Proofs/Implementation}{17}{0}{2}
\beamer@sectionintoc {3}{Summary}{21}{0}{3}
\beamer@subsectionintoc {3}{1}{Summary}{21}{0}{3}
\beamer@subsectionintoc {3}{2}{Future work}{23}{0}{3}
